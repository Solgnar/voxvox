mv_import 2048

//Stripe1
0 0 0 $\model\W5a.vox
126 0 0 $\model\W5b.vox
252 0 0 $\model\W5c.vox
378 0 0 $\model\W5d.vox
504 0 0 $\model\W5e.vox
630 0 0 $\model\W5f.vox

//Stripe2
0 126 0 $\model\W5g.vox
126 126 0 $\model\W5h.vox
252 126 0 $\model\W5i.vox
378 126 0 $\model\W5j.vox
504 126 0 $\model\W5k.vox
630 126 0 $\model\W5l.vox

//Stripe3
0 252 0 $\model\W5m.vox
126 252 0 $\model\W5n.vox
252 252 0 $\model\W5o.vox
378 252 0 $\model\W5p.vox
504 252 0 $\model\W5q.vox
630 252 0 $\model\W5r.vox

//Stripe4
0 378 0 $\model\W5s.vox
126 378 0 $\model\W5t.vox
252 378 0 $\model\W5u.vox
378 378 0 $\model\W5v.vox
504 378 0 $\model\W5w.vox
630 378 0 $\model\W5x.vox

//Stripe5
0 504 0 $\model\W5y.vox
126 504 0 $\model\W5z.vox
252 504 0 $\model\W5aa.vox
378 504 0 $\model\W5ab.vox
504 504 0 $\model\W5ac.vox
630 504 0 $\model\W5ad.vox

//Stripe6
0 630 0 $\model\W5ae.vox
126 630 0 $\model\W5af.vox
252 630 0 $\model\W5ag.vox
378 630 0 $\model\W5ah.vox
504 630 0 $\model\W5ai.vox
630 630 0 $\model\W5aj.vox
