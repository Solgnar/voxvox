mv_import 2048

//Stripe1
0 0 0 $\model\W2a.vox
126 0 0 $\model\W2b.vox
252 0 0 $\model\W2c.vox
378 0 0 $\model\W2d.vox
504 0 0 $\model\W2e.vox
630 0 0 $\model\W2f.vox

//Stripe2
0 126 0 $\model\W2g.vox
126 126 0 $\model\W2h.vox
252 126 0 $\model\W2i.vox
378 126 0 $\model\W2j.vox
504 126 0 $\model\W2k.vox
630 126 0 $\model\W2l.vox

//Stripe3
0 252 0 $\model\W2m.vox
126 252 0 $\model\W2n.vox
252 252 0 $\model\W2o.vox
378 252 0 $\model\W2p.vox
504 252 0 $\model\W2q.vox
630 252 0 $\model\W2r.vox

//Stripe4
0 378 0 $\model\W2s.vox
126 378 0 $\model\W2t.vox
252 378 0 $\model\W2u.vox
378 378 0 $\model\W2v.vox
504 378 0 $\model\W2w.vox
630 378 0 $\model\W2x.vox

//Stripe5
0 504 0 $\model\W2y.vox
126 504 0 $\model\W2z.vox
252 504 0 $\model\W2aa.vox
378 504 0 $\model\W2ab.vox
504 504 0 $\model\W2ac.vox
630 504 0 $\model\W2ad.vox

//Stripe6
0 630 0 $\model\W2ae.vox
126 630 0 $\model\W2af.vox
252 630 0 $\model\W2ag.vox
378 630 0 $\model\W2ah.vox
504 630 0 $\model\W2ai.vox
630 630 0 $\model\W2aj.vox
