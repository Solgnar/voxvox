// [EPHTARCY 10/12/2015]

//========
// block:data | rgb | name
//========
0:0     0 0 0           Air
1:0     125 125 125     Stone
1:1     153 113 98      Granite
1:2     159 114 98      Polished Granite
1:3     179 179 182     Diorite
1:4     183 183 185     Polished Diorite
1:5     130 131 131     Andesite
1:6     133 133 134     Polished Andesite
2:0     118 179 76      Grass
3:0     134 96 67       Dirt
3:1     134 96 67       Coarse Dirt
3:2     134 96 67       Podzol
4:0     122 122 122     Cobblestone
5:0     156 127 78      Wooden Plank (Oak)
5:1     103 77 46       Wooden Plank (Spruce)
5:2     195 179 123     Wooden Plank (Birch)
5:3     154 110 77      Wooden Plank (Jungle)
5:4     169 91 51       Wooden Plank (Acacia)
5:5     61 39 18        Wooden Plank (Dark Oak)
6:0     71 102 37       Sapling (Oak)
6:1     51 58 33        Sapling (Spruce)
6:2     118 150 84      Sapling (Birch)
6:3     48 86 18        Sapling (Jungle)
6:4     114 115 20      Sapling (Acacia)
6:5     56 86 28        Sapling (Dark Oak)
7:0     83 83 83        Bedrock
8:0     112 175 220     Water
9:0     112 175 220     Water (No Spread)
10:0    207 91 19       Lava
11:0    212 90 18       Lava (No Spread)
12:0    219 211 160     Sand
12:1    167 87 32       Red Sand
13:0    126 124 122     Gravel
14:0    143 139 124     Gold Ore
15:0    135 130 126     Iron Ore
16:0    115 115 115     Coal Ore
17:0    102 81 49       Wood (Oak)
17:1    45 28 12        Wood (Spruce)
17:2    206 206 201     Wood (Birch)
17:3    87 67 26        Wood (Jungle)
17:4    88 69 39        Wood (Oak 4)
17:5    36 20 5         Wood (Oak 5)
18:0    69 178 49       Leaves (Oak)
18:1    116 116 116     Leaves (Spruce)
18:2    135 135 135     Leaves (Birch)
18:3    45 125 16       Leaves (Jungle)
19:0    194 195 84      Sponge
19:1    153 148 53      Wet Sponge
20:0    218 240 244     Glass
21:0    102 112 134     Lapis Lazuli Ore
22:0    38 67 137       Lapis Lazuli Block
23:0    131 131 131     Dispenser
24:0    220 211 159     Sandstone
24:1    220 211 159     Sandstone (Chiseled)
24:2    220 211 159     Sandstone (Smooth)
25:0    100 67 50       Note Block
26:0    180 52 54       Bed (Block)
27:0    246 199 35      Rail (Powered)
28:0    124 124 124     Rail (Detector)
29:0    142 191 119     Sticky Piston
30:0    220 220 220     Cobweb
31:0    149 101 41      Tall Grass (Dead Shrub)
31:1    2 145 36        Tall Grass
31:2    91 125 56       Tall Grass (Fern)
32:0    123 79 25       Dead Shrub
33:0    165 139 83      Piston
34:0    189 150 94      Piston (Head)
35:0    221 221 221     Wool
35:1    219 125 62      Orange Wool
35:2    179 80 188      Magenta Wool
35:3    106 138 201     Light Blue Wool
35:4    177 166 39      Yellow Wool
35:5    65 174 56       Lime Wool
35:6    208 132 153     Pink Wool
35:7    64 64 64        Gray Wool
35:8    154 161 161     Light Gray Wool
35:9    46 110 137      Cyan Wool
35:10   126 61 181      Purple Wool
35:11   46 56 141       Blue Wool
35:12   79 50 31        Brown Wool
35:13   53 70 27        Green Wool
35:14   150 52 48       Red Wool
35:15   25 22 22        Black Wool
36:0    229 229 229     Piston (Moving)
37:0    255 255 0       Dandelion
38:0    218 0 13        Poppy
38:1    37 152 138      Blue Orchid
38:2    177 141 211     Allium
38:3    255 255 167     Azure Bluet
38:4    208 57 22       Red Tulip
38:5    95 134 32       Orange Tulip
38:6    94 153 65       White Tulip
38:7    101 150 73      Pink Tulip
38:8    176 197 139     Oxeye Daisy
39:0    120 87 65       Brown Mushroom
40:0    225 15 13       Red Mushroom
41:0    249 236 78      Block of Gold
42:0    219 219 219     Block of Iron
43:0    161 161 161     Stone Slab (Double)
43:1    223 216 164     Sandstone Slab (Double)
43:2    146 121 68      Wooden Slab (Double)
43:3    152 152 152     Cobblestone Slab (Double)
43:4    225 104 73      Brick Slab (Double)
43:5    120 120 120     Stone Brick Slab (Double)
43:6    55 24 29        Nether Brick Slab (Double)
43:7    234 230 224     Quartz Slab (Double)
43:8    168 168 168     Smooth Stone Slab (Double)
43:9    223 216 163     Smooth Sandstone Slab (Double)
44:0    166 166 166     Stone Slab
44:1    220 212 162     Sandstone Slab
44:2    201 162 101     Wooden Slab
44:3    109 109 109     Cobblestone Slab
44:4    147 80 65       Brick Slab
44:5    128 128 128     Stone Brick Slab
44:6    70 34 41        Nether Brick Slab
44:7    237 235 228     Quartz Slab
45:0    188 48 6        Brick
46:0    175 38 0        TNT
47:0    107 88 57       Bookshelf
48:0    101 135 101     Moss Stone
49:0    20 18 29        Obsidian
50:0    255 255 0       Torch
51:0    222 95 0        Fire
52:0    26 39 49        Mob Spawner
53:0    166 135 78      Wooden Stairs (Oak)
54:0    164 116 42      Chest
55:0    255 0 0         Redstone Wire
56:0    129 140 143     Diamond Ore
57:0    97 219 213      Block of Diamond
58:0    136 80 46       Workbench
59:0    60 89 23        Wheat (Crop)
60:0    130 86 51       Farmland
61:0    103 103 103     Furnace
62:0    255 185 0       Furnace (Smelting)
63:0    184 154 91      Sign (Block)
64:0    148 115 56      Wood Door (Block)
65:0    121 95 52       Ladder
66:0    182 144 81      Rail
67:0    106 106 106     Cobblestone Stairs
68:0    184 154 91      Sign (Wall Block)
69:0    106 89 64       Lever
70:0    122 122 122     Stone Pressure Plate
71:0    194 194 194     Iron Door (Block)
72:0    201 160 101     Wooden Pressure Plate
73:0    132 107 107     Redstone Ore
74:0    221 45 45       Redstone Ore (Glowing)
75:0    102 0 0         Redstone Torch (Off)
76:0    255 97 0        Redstone Torch
77:0    132 132 132     Button (Stone)
78:0    223 241 241     Snow
79:0    125 173 255     Ice
80:0    239 251 251     Snow Block
81:0    15 131 29       Cactus
82:0    158 164 176     Clay Block
83:0    148 192 101     Sugar Cane (Block)
84:0    133 89 59       Jukebox
85:0    141 116 66      Fence (Oak)
86:0    227 140 27      Pumpkin
87:0    111 54 52       Netherrack
88:0    84 64 51        Soul Sand
89:0    143 118 69      Glowstone
90:0    87 10 191       Portal
91:0    241 152 33      Jack-O-Lantern
92:0    236 255 255     Cake (Block)
93:0    178 178 178     Redstone Repeater (Block Off)
94:0    178 178 178     Redstone Repeater (Block On)
95:0    255 255 255     Stained Glass (White)
95:1    216 127 51      Stained Glass (Orange)
95:2    178 76 216      Stained Glass (Magenta)
95:3    102 153 216     Stained Glass (Light Blue)
95:4    229 229 51      Stained Glass (Yellow)
95:5    127 204 25      Stained Glass (Lime)
95:6    242 127 165     Stained Glass (Pink)
95:7    76 76 76        Stained Glass (Gray)
95:8    117 117 117     Stained Glass (Light Grey)
95:9    76 127 153      Stained Glass (Cyan)
95:10   127 63 178      Stained Glass (Purple)
95:11   51 76 178       Stained Glass (Blue)
95:12   102 76 51       Stained Glass (Brown)
95:13   102 127 51      Stained Glass (Green)
95:14   153 51 51       Stained Glass (Red)
95:15   25 25 25        Stained Glass (Black)
96:0    126 93 45       Trapdoor
97:0    124 124 124     Monster Egg (Stone)
97:1    141 141 141     Monster Egg (Cobblestone)
97:2    122 122 122     Monster Egg (Stone Brick)
97:3    105 120 81      Monster Egg (Mossy Stone Brick)
97:4    104 104 104     Monster Egg (Cracked Stone)
97:5    118 118 118     Monster Egg (Chiseled Stone)
98:0    122 122 122     Stone Bricks
98:1    122 122 122     Mossy Stone Bricks
98:2    122 122 122     Cracked Stone Bricks
98:3    122 122 122     Chiseled Stone Brick
99:0    207 175 124     Brown Mushroom (Block)
100:0   202 170 120     Red Mushroom (Block)
101:0   109 108 106     Iron Bars
102:0   211 239 244     Glass Pane
103:0   196 189 40      Melon (Block)
104:0   146 221 105     Pumpkin Vine
105:0   115 174 83      Melon Vine
106:0   32 81 12        Vines
107:0   165 135 82      Fence Gate (Oak)
108:0   148 64 42       Brick Stairs
109:0   122 122 122     Stone Brick Stairs
110:0   138 113 117     Mycelium
111:0   118 118 118     Lily Pad
112:0   44 22 26        Nether Brick
113:0   44 22 26        Nether Brick Fence
114:0   44 22 26        Nether Brick Stairs
115:0   166 40 45       Nether Wart
116:0   84 196 177      Enchantment Table
117:0   124 103 81      Brewing Stand (Block)
118:0   73 73 73        Cauldron (Block)
119:0   52 52 52        End Portal
120:0   52 137 209      End Portal Frame
121:0   221 223 165     End Stone
122:0   12 9 15         Dragon Egg
123:0   151 99 49       Redstone Lamp
124:0   227 160 66      Redstone Lamp (On)
125:0   161 132 77      Oak-Wood Slab (Double)
125:1   125 90 54       Spruce-Wood Slab (Double)
125:2   215 204 141     Birch-Wood Slab (Double)
125:3   183 133 96      Jungle-Wood Slab (Double)
125:4   169 88 48       Acacia Wood Slab (Double)
125:5   67 42 21        Dark Oak Wood Slab (Double)
126:0   158 133 73      Oak-Wood Slab
126:1   100 79 46       Spruce-Wood Slab
126:2   235 225 155     Birch-Wood Slab
126:3   139 97 60       Jungle-Wood Slab
126:4   171 92 51       Acacia Wood Slab
126:5   66 42 18        Dark Oak Wood Slab
127:0   221 113 31      Cocoa Plant
128:0   231 226 168     Sandstone Stairs
129:0   109 128 116     Emerald Ore
130:0   42 58 60        Ender Chest
131:0   124 124 124     Tripwire Hook
132:0   90 90 90        Tripwire
133:0   81 217 117      Block of Emerald
134:0   129 94 52       Wooden Stairs (Spruce)
135:0   206 192 132     Wooden Stairs (Birch)
136:0   136 95 69       Wooden Stairs (Jungle)
137:0   142 139 134     Command Block
138:0   116 221 215     Beacon
139:0   89 89 89        Cobblestone Wall
139:1   42 94 42        Mossy Cobblestone Wall
140:0   118 65 51       Flower Pot (Block)
141:0   10 140 0        Carrot (Crop)
142:0   4 164 23        Potatoes (Crop)
143:0   179 146 89      Button (Wood)
144:0   176 176 176     Head Block (Skeleton)
144:1   79 85 85        Head Block (Wither)
144:2   98 146 75       Head Block (Zombie)
144:3   204 151 126     Head Block (Steve)
144:4   82 175 67       Head Block (Creeper)
145:0   71 67 67        Anvil
145:1   71 67 67        Anvil (Slightly Damaged)
145:2   71 67 67        Anvil (Very Damaged)
146:0   158 107 29      Trapped Chest
147:0   254 253 112     Weighted Pressure Plate (Light)
148:0   229 229 229     Weighted Pressure Plate (Heavy)
149:0   75 74 76        Redstone Comparator (Off)
150:0   191 198 189     Redstone Comparator (On)
151:0   251 237 221     Daylight Sensor
152:0   171 27 9        Block of Redstone
153:0   125 84 79       Nether Quartz Ore
154:0   113 113 113     Hopper
155:0   234 230 223     Quartz Block
155:1   224 219 210     Chiseled Quartz Block
155:2   234 231 225     Pillar Quartz Block
156:0   235 232 227     Quartz Stairs
157:0   155 129 65      Rail (Activator)
158:0   116 116 116     Dropper
159:0   209 178 161     Stained Clay (White)
159:1   161 83 37       Stained Clay (Orange)
159:2   149 88 108      Stained Clay (Magenta)
159:3   113 108 137     Stained Clay (Light Blue)
159:4   186 133 35      Stained Clay (Yellow)
159:5   103 117 52      Stained Clay (Lime)
159:6   161 78 78       Stained Clay (Pink)
159:7   57 42 35        Stained Clay (Gray)
159:8   135 104 95      Stained Clay (Light Gray)
159:9   86 91 91        Stained Clay (Cyan)
159:10  118 70 86       Stained Clay (Purple)
159:11  74 59 91        Stained Clay (Blue)
159:12  77 51 35        Stained Clay (Brown)
159:13  76 83 42        Stained Clay (Green)
159:14  143 61 46       Stained Clay (Red)
159:15  37 22 16        Stained Clay (Black)
160:0   246 246 246     Stained Glass Pane (White)
160:1   208 122 48      Stained Glass Pane (Orange)
160:2   171 73 208      Stained Glass Pane (Magenta)
160:3   97 147 208      Stained Glass Pane (Light Blue)
160:4   221 221 48      Stained Glass Pane (Yellow)
160:5   122 196 24      Stained Glass Pane (Lime)
160:6   233 122 159     Stained Glass Pane (Pink)
160:7   73 73 73        Stained Glass Pane (Gray)
160:8   145 145 145     Stained Glass Pane (Light Gray)
160:9   73 122 147      Stained Glass Pane (Cyan)
160:10  122 61 171      Stained Glass Pane (Purple)
160:11  48 73 171       Stained Glass Pane (Blue)
160:12  97 73 48        Stained Glass Pane (Brown)
160:13  97 122 48       Stained Glass Pane (Green)
160:14  147 48 48       Stained Glass Pane (Red)
160:15  24 24 24        Stained Glass Pane (Black)
161:0   135 135 135     Leaves (Acacia)
161:1   55 104 33       Leaves (Dark Oak)
162:0   176 90 57       Wood (Acacia Oak)
162:1   93 74 49        Wood (Dark Oak)
163:0   172 92 50       Wooden Stairs (Acacia)
164:0   71 44 21        Wooden Stairs (Dark Oak)
165:0   120 200 101     Slime Block
166:0   223 52 53       Barrier
167:0   199 199 199     Iron Trapdoor
168:0   114 175 165     Prismarine
168:1   92 158 143      Prismarine Bricks
168:2   72 106 94       Dark Prismarine
169:0   172 199 190     Sea Lantern
170:0   220 211 159     Hay Bale
171:0   202 202 202     Carpet (White)
171:1   221 133 75      Carpet (Orange)
171:2   177 67 186      Carpet (Magenta)
171:3   75 113 189      Carpet (Light Blue)
171:4   197 183 44      Carpet (Yellow)
171:5   60 161 51       Carpet (Lime)
171:6   206 142 168     Carpet (Pink)
171:7   70 70 70        Carpet (Grey)
171:8   162 162 162     Carpet (Light Gray)
171:9   48 116 145      Carpet (Cyan)
171:10  148 81 202      Carpet (Purple)
171:11  54 69 171       Carpet (Blue)
171:12  82 52 32        Carpet (Brown)
171:13  62 85 33        Carpet (Green)
171:14  187 61 57       Carpet (Red)
171:15  35 31 31        Carpet (Black)
172:0   150 92 66       Hardened Clay
173:0   18 18 18        Block of Coal
174:0   162 191 244     Packed Ice
175:0   207 116 20      Sunflower
175:1   168 112 178     Lilac
175:2   102 158 88      Double Tallgrass
175:3   84 129 72       Large Fern
175:4   215 2 8         Rose Bush
175:5   192 150 207     Peony
176:0   240 240 240     Standing Banner (Block)
177:0   240 240 240     Wall Banner (Block)
178:0   240 240 240     Inverted Daylight Sensor
179:0   172 86 29       Red Sandstone
179:1   172 86 29       Red Sandstone (Chiseled)
179:2   172 86 29       Red Sandstone (Smooth)
180:0   174 87 29       Red Sandstone Stairs
181:0   174 87 29       Red Sandstone Slab (Double)
182:0   174 87 29       Red Sandstone Slab
183:0   80 60 36        Fence Gate (Spruce)
184:0   221 205 141     Fence Gate (Birch)
185:0   175 122 77      Fence Gate (Jungle)
186:0   52 32 14        Fence Gate (Dark Oak)
187:0   207 107 54      Fence Gate (Acacia)
188:0   126 93 53       Fence (Spruce)
189:0   199 184 123     Fence (Birch)
190:0   187 134 95      Fence (Jungle)
191:0   63 46 30        Fence (Dark Oak)
192:0   197 107 58      Fence (Acacia)
193:0   110 83 48       Wooden Door Block (Spruce)
194:0   247 243 224     Wooden Door Block (Birch)
195:0   169 119 80      Wooden Door Block (Jungle)
196:0   170 85 41       Wooden Door Block (Acacia)
197:0   78 55 33        Wooden Door Block (Dark Oak)
198:0   220 197 205     End rod
199:0   96 59 96        Chorus Plant
200:0   133 103 133     Chorus Flower
201:0   166 121 166     Purpur Block
202:0   170 126 170     Purpur Pillar
203:0   168 121 168     Purpur Stairs
204:0   168 121 168     Purpur Slab (Double)
205:0   168 121 168     Purpur Slab
206:0   225 230 170     End Stone Bricks
208:0   152 125 69      Grass Path
209:0   240 240 240     End Gateway
255:0   50 33 36        Structure Block