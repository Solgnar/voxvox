================================
MagicaVoxel Viewer : Voxel Renderer
================================
date    : 03/07/2016

version : 0.41

os      : Win32, MacOS 10.7+

website : https://ephtracy.github.io

demo    : https://youtu.be/oedEhpRgqG4

twitter : @ ephtracy


================================
Model Credits
================================
biome.vox
  : twitter @ y2bcrazy

wild-western-town.schematic
  : youtube @ jar9 : https://www.youtube.com/channel/UCmsDsjHwjAH9IHq5mlNudUQ

rungholt.vox
  : kescha : http://graphics.cs.williams.edu/data/meshes.xml

monu9.vox
  : based on the game "monument valley"

xyzrgb_dragon_16k.rsvo
  : The Stanford 3D Scanning Repository : http://graphics.stanford.edu/data/3Dscanrep

christchurch.png
  : http://johnflower.org/tutorial/getting-height-maps-qgis

tq3179_DSM_1m.asc
  : Environment Agency : https://environmentagency.blog.gov.uk/2015/09/18/laser-surveys-light-up-open-data




