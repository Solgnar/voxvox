mv_import 2048

//Stripe1
0 0 0 $\model\W6a.vox
126 0 0 $\model\W6b.vox
252 0 0 $\model\W6c.vox
378 0 0 $\model\W6d.vox
504 0 0 $\model\W6e.vox
630 0 0 $\model\W6f.vox

//Stripe2
0 126 0 $\model\W6g.vox
126 126 0 $\model\W6h.vox
252 126 0 $\model\W6i.vox
378 126 0 $\model\W6j.vox
504 126 0 $\model\W6k.vox
630 126 0 $\model\W6l.vox

//Stripe3
0 252 0 $\model\W6m.vox
126 252 0 $\model\W6n.vox
252 252 0 $\model\W6o.vox
378 252 0 $\model\W6p.vox
504 252 0 $\model\W6q.vox
630 252 0 $\model\W6r.vox

//Stripe4
0 378 0 $\model\W6s.vox
126 378 0 $\model\W6t.vox
252 378 0 $\model\W6u.vox
378 378 0 $\model\W6v.vox
504 378 0 $\model\W6w.vox
630 378 0 $\model\W6x.vox

//Stripe5
0 504 0 $\model\W6y.vox
126 504 0 $\model\W6z.vox
252 504 0 $\model\W6aa.vox
378 504 0 $\model\W6ab.vox
504 504 0 $\model\W6ac.vox
630 504 0 $\model\W6ad.vox

//Stripe6
0 630 0 $\model\W6ae.vox
126 630 0 $\model\W6af.vox
252 630 0 $\model\W6ag.vox
378 630 0 $\model\W6ah.vox
504 630 0 $\model\W6ai.vox
630 630 0 $\model\W6aj.vox
